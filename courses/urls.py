
from django.urls import path

from courses.views import CourseListView, CourseUpdateView, CourseCreateView, CourseDeleteView

urlpatterns = [
    path('', CourseListView.as_view(), name='courses_list'),
    path('create', CourseCreateView.as_view(), name='create_course'),
    path('update/<int:courses_id>', CourseUpdateView.as_view(), name='update_course'),
    path('delete/<int:courses_id>', CourseDeleteView.as_view(), name='delete_course'),
]
