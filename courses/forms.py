from django.forms import ModelForm

import django_filters

from courses.models import Courses


class CourseFilter(django_filters.FilterSet):
    class Meta:
        model = Courses
        fields = {
            'name': ['exact', 'icontains'],
            'group': ['exact'],
            'teacher': ['exact'],

        }


class CourseBaseForm(ModelForm):
    class Meta:
        model = Courses
        fields = "__all__"


class CourseCreateForm(CourseBaseForm):
    pass


class CourseUpdateForm(CourseBaseForm):
    class Meta(CourseBaseForm.Meta):
        pass
