from django.contrib import admin    # noqa

# Register your models here.
from courses.models import Courses

admin.site.register(Courses)
