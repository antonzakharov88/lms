from django.db import models

from groups.models import Group

from teachers.models import Teacher


class Courses(models.Model):
    class DIFFICULTY(models.IntegerChoices):
        TRIVIAL = 0, "Trivial"
        EASY = 1, "Easy"
        MEDIUM = 2, "Medium"
        EXPERT = 3, "Expert"
        HARDCORE = 4, "Hardcore"
    name = models.CharField(max_length=64, null=False)
    group = models.OneToOneField(
        to=Group,
        related_name="course_group",
        null=True,
        on_delete=models.SET_NULL
    )
    teacher = models.ManyToManyField(
        to=Teacher,
        related_name="course_teacher"
    )
    difficulty = models.PositiveSmallIntegerField(
        choices=DIFFICULTY.choices,
        default=DIFFICULTY.TRIVIAL
    )

    def __str__(self):
        return f'{self.name}'
