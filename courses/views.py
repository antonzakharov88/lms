from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, UpdateView, CreateView, DeleteView

from courses.forms import CourseUpdateForm, CourseCreateForm, CourseFilter
from courses.models import Courses


def get_courses(request):
    courses = Courses.objects.all().order_by('-id')

    filter = CourseFilter(       # noqa
        data=request.GET,
        queryset=courses
    )
    return render(request=request,
                  template_name='courses_list.html',
                  context={'filter': filter}
                  )


def create_course(request):
    if request.method == 'POST':

        form = CourseCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('courses_list'))

    else:

        form = CourseCreateForm()

    return render(request,
                  template_name='course_create.html',
                  context={'form': form}
                  )


def update_course(request, courses_id):
    course = Courses.objects.get(id=courses_id)

    if request.method == 'POST':

        form = CourseUpdateForm(
            data=request.POST,
            instance=course)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('courses_list'))

    else:

        form = CourseUpdateForm(instance=course)

    return render(request,
                  template_name='course_update.html',
                  context={'form': form,
                           'course': course,
                           }
                  )


def delete_course(request, course_id):
    course = get_object_or_404(Courses, id=course_id)

    if request.method == 'POST':
        course.delete()

        return HttpResponseRedirect(reverse('courses_list'))

    return render(request,
                  template_name='course_delete.html',
                  context={'course': course}
                  )


class CourseListView(ListView):
    model = Courses
    template_name = "courses:list.html"

    def get_filter(self):
        return CourseFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        #        context['teachers'] = self.get_filter().qs.select_related('group').prefetch_related('headed_group')
        return context


class CourseUpdateView(UpdateView):
    model = Courses
    form_class = CourseUpdateForm
    success_url = reverse_lazy('courses_list')
    template_name = 'course:update.html'
    pk_url_kwarg = 'courses_id'


class CourseCreateView(CreateView):
    model = Courses
    form_class = CourseCreateForm
    success_url = reverse_lazy('courses_list')
    template_name = 'course:create.html'


class CourseDeleteView(DeleteView):
    model = Courses
    success_url = reverse_lazy('courses_list')
    template_name = 'course:delete.html'
    pk_url_kwarg = 'courses_id'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)
