class frange:  # noqa
    def __init__(self, start, stop=None, step=1):
        self.start = start
        self.stop = stop
        self.step = step

        if self.stop is None:
            self.stop = start
            self.start = 0

    def __next__(self):
        if (self.step > 0 and self.start < self.stop) or (self.step < 0 and self.start > self.stop):
            result = self.start
            self.start += self.step
            return result

        else:
            raise StopIteration

    def __iter__(self):
        return self


class imap:  # noqa
    def __init__(self, func, *args):
        self.func = func
        self._args_list = [iter(arg) for arg in args]

        if self.func is None or self._args_list is None:
            raise AttributeError('Imap must have two or more parameters')

    def __next__(self):
        list_iterables = []
        for i in range(len(self._args_list)):
            list_iterables.append(next(self._args_list[i]))

        return self.func(*list_iterables)

    def __iter__(self):
        return self
