import django_filters

from django.core.exceptions import ValidationError
from django.forms import ModelForm

from students.models import Student


class StudentFilter(django_filters.FilterSet):
    class Meta:
        model = Student
        # fields = ['first_name', 'last_name', 'age']
        fields = {
            'age': ['lt', 'gt'],
            'first_name': ['exact', 'icontains'],
            'last_name': ['exact', 'startswith'],
        }


class StudentBaseForm(ModelForm):
    class Meta:
        model = Student
        fields = '__all__'

    def clean_email_field(self):
        new_email = self.cleaned_data['email_field']
        exists = Student.objects.filter(email_field=new_email).exclude(id=self.instance.id)
        if exists:
            raise ValidationError('Email already exists')
        return new_email

    def clean(self):
        result = super().clean()
        enroll_date = self.cleaned_data['enroll_date']
        graduate_date = self.cleaned_data['graduate_day']

        if enroll_date > graduate_date:
            raise ValidationError('Enroll date coudn\'t be after graduate date')

        return result


class StudentCreateForm(StudentBaseForm):
    pass


class StudentUpdateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        exclude = ['age']
