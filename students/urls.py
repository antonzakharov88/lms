from django.contrib import admin # noqa
from django.urls import path

from students.views import StudentUpdateView, StudentListView, StudentsCreateView, StudentDeleteView

urlpatterns = [

    path('', StudentListView.as_view(), name='list'),
    path('create', StudentsCreateView.as_view(), name='create'),
    path('update/<int:student_id>', StudentUpdateView.as_view(), name='update'),
    path('delete/<int:student_id>', StudentDeleteView.as_view(), name='delete'),

]
