import datetime
import random

import django
from django.db import models


from core.models import Person

from groups.models import Group


# Create your models here.
class Student(Person):

    group = models.ForeignKey(
        to=Group,
        on_delete=models.SET_NULL,
        null=True,
        related_name='students'
    )

    enroll_date = models.DateField(default=datetime.date.today)
    graduate_day = models.DateField(default=django.utils.timezone.now)

    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.age},' \
               f' {self.email_field}, {self.phone_number}'

    @classmethod
    def _generate(cls):
        obj = super()._generate()
        obj.group = random.choice(Group.objects.all())
        obj.save()
