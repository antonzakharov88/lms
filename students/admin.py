from django.contrib import admin  # noqa

# Register your models here.
from students.models import Student


class StudentAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['first_name', 'last_name', 'age']
    search_fields = ['first_name', 'last_name']


admin.site.register(Student, StudentAdmin)
