import itertools

from students.utils import frange, imap

assert (list(frange(5)) == [0, 1, 2, 3, 4])
assert (list(frange(2, 5)) == [2, 3, 4])
assert (list(frange(2, 10, 2)) == [2, 4, 6, 8])
assert (list(frange(10, 2, -2)) == [10, 8, 6, 4])
assert (list(frange(2, 5.5, 1.5)) == [2, 3.5, 5])
assert (list(frange(1, 5)) == [1, 2, 3, 4])
assert (list(frange(0, 5)) == [0, 1, 2, 3, 4])
assert (list(frange(0, 0)) == [])
assert (list(frange(100, 0)) == [])
assert (list(itertools.islice(frange(0, float(10 ** 10), 1.0), 0, 4)) == [0, 1.0, 2.0, 3.0])

print('frange SUCCESS!')

nums = [1, 2, 3, 4, 5]
nums2 = [100, 200, 300, 400, 500]
list_of_words = ['one', 'two', 'list', '', 'dict']
list_of_str = ['1', '2', '5', '10']
vlans = [100, 110, 150, 200, 201, 202]

assert (list(imap(lambda x, y: x * y, nums, nums2))) == [100, 400, 900, 1600, 2500]
assert (list(imap(lambda x: x * 3 + 3, [2, 3, 4, 5, 6]))) == [9, 12, 15, 18, 21]
assert (list(imap(ord, 'Sample text'))) == [83, 97, 109, 112, 108, 101, 32, 116, 101, 120, 116]
assert (list(imap(abs, (-2.2, 15, -26, 3.3, 0)))) == [2.2, 15, 26, 3.3, 0]
assert (list(imap(str.upper, list_of_words))) == ['ONE', 'TWO', 'LIST', '', 'DICT']
assert (list(imap(int, list_of_str))) == [1, 2, 5, 10]
assert (list(imap(lambda x: 'vlan {}'.format(x), vlans))) == ['vlan 100', 'vlan 110', 'vlan 150', 'vlan 200',
                                                              'vlan 201', 'vlan 202']

print('imap SUCCESS!')
