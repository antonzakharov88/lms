from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import UpdateView, ListView, CreateView, DeleteView

from students.forms import StudentCreateForm, StudentUpdateForm, StudentFilter
from students.models import Student


def get_students(request):
    students = Student.objects.all().select_related('group', 'headed_group').order_by("id")

    filter = StudentFilter(  # noqa
        data=request.GET,
        queryset=students
    )

    return render(request=request,
                  template_name='students_list.html',
                  context={'filter': filter}
                  )


def create_student(request):
    if request.method == 'POST':

        form = StudentCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('list'))

    else:

        form = StudentCreateForm()

    return render(request,
                  template_name='students_create.html',
                  context={'form': form}
                  )


def update_student(request, student_id):
    student = Student.objects.get(id=student_id)

    if request.method == 'POST':

        form = StudentUpdateForm(
            data=request.POST,
            instance=student)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('list'))

    else:

        form = StudentUpdateForm(instance=student)

    return render(request,
                  template_name='students_update.html',
                  context={'form': form}
                  )


def delete_student(request, student_id):
    student = get_object_or_404(Student, id=student_id)

    if request.method == 'POST':
        student.delete()

        return HttpResponseRedirect(reverse('list'))

    return render(request,
                  template_name='students_delete.html',
                  context={'student': student}
                  )


class StudentUpdateView(UpdateView):
    model = Student
    form_class = StudentUpdateForm
    success_url = reverse_lazy('list')
    template_name = 'students:update.html'
    pk_url_kwarg = 'student_id'


class StudentListView(LoginRequiredMixin, ListView):
    model = Student
    template_name = 'students:list.html'
    login_url = reverse_lazy('accounts:login')
    paginate_by = 5
    context_object_name = 'students'

    def get_filter(self):
        return StudentFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs.select_related('group', 'headed_group')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()

        return context


class StudentsCreateView(CreateView):
    model = Student
    form_class = StudentCreateForm
    success_url = reverse_lazy('list')
    template_name = 'students:create.html'


class StudentDeleteView(DeleteView):
    model = Student
    success_url = reverse_lazy('list')
    template_name = 'students:delete.html'
    pk_url_kwarg = 'student_id'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)
