import django_filters

from django.core.exceptions import ValidationError
from django.forms import ModelForm

from teachers.models import Teacher


class TeacherFilter(django_filters.FilterSet):
    class Meta:
        model = Teacher
        # fields = ['first_name', 'last_name', 'age']
        fields = {
            'first_name': ['exact', 'icontains'],
            'last_name': ['exact', 'icontains'],
            'phone_number': ['icontains']
        }


class TeacherBaseForm(ModelForm):
    class Meta:
        model = Teacher
        fields = "__all__"

    def clean_email(self):
        new_email = self.cleaned_data['email']
        exists = Teacher.objects.filter(email=new_email).exclude(id=self.instance.id)
        if exists:
            raise ValidationError('Email already exists')
        return new_email


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    pass
