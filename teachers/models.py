import random

# Create your models here.
from django.db import models

from core.models import Person

from groups.models import Group


class Teacher(Person):
    salary = models.PositiveIntegerField(default=42)
    group = models.ForeignKey(
        to=Group,
        on_delete=models.SET_NULL,
        null=True,
        related_name="group_teacher"
    )

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    @classmethod
    def _generate(cls):
        obj = super()._generate()
        obj.salary = random.randint(100, 1000)
        obj.save()
