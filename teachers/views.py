from copy import copy

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from teachers.forms import TeacherCreateForm, TeacherUpdateForm, TeacherFilter
from teachers.models import Teacher


def get_teachers(request):
    teachers = Teacher.objects.all().order_by('-id')

    filter = TeacherFilter(  # noqa
        data=request.GET,
        queryset=teachers
    )
    return render(request=request,
                  template_name='teachers_list.html',
                  context={'filter': filter}
                  )


def create_teacher(request):
    if request.method == 'POST':

        form = TeacherCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('list_teachers'))
    else:

        form = TeacherCreateForm()

    return render(request,
                  template_name='teachers_create.html',
                  context={'form': form}
                  )


def update_teacher(request, teacher_id):
    teacher = Teacher.objects.get(id=teacher_id)

    if request.method == 'POST':

        form = TeacherUpdateForm(
            data=request.POST,
            instance=teacher)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('list_teachers'))

    elif request.method == 'GET':

        form = TeacherUpdateForm(instance=teacher)

    return render(request,
                  template_name='teachers_update.html',
                  context={'form': form}
                  )


def delete_teacher(request, teacher_id):
    teacher = get_object_or_404(Teacher, id=teacher_id)

    if request.method == 'POST':
        teacher.delete()

        return HttpResponseRedirect(reverse('list_teachers'))

    return render(request,
                  template_name='teacher_delete.html',
                  context={'teacher': teacher}
                  )


class TeacherListView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teachers:list.html"
    login_url = reverse_lazy('accounts:login')
    paginate_by = 5
    context_object_name = 'teachers'

    def get_filter(self):
        return TeacherFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs.select_related('group')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        context['query_params'] = self.request.GET.urlencode()
        if 'page' in self.request.GET:
            get_params = copy(self.request.GET)
            del get_params['page']
            context['query_params'] = get_params.urlencode()
        return context


class TeachersCreateView(CreateView):
    model = Teacher
    form_class = TeacherCreateForm
    success_url = reverse_lazy('list_teachers')
    template_name = 'teachers:create.html'


class TeacherUpdateView(UpdateView):
    model = Teacher
    form_class = TeacherUpdateForm
    success_url = reverse_lazy('list_teachers')
    template_name = 'teachers:update.html'
    pk_url_kwarg = 'teacher_id'


class TeacherDeleteView(DeleteView):
    model = Teacher
    success_url = reverse_lazy('list_teachers')
    template_name = 'teacher:delete.html'
    pk_url_kwarg = 'teacher_id'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)
