
from django.urls import path


from teachers.views import TeacherListView, TeachersCreateView, TeacherUpdateView, TeacherDeleteView

urlpatterns = [
    path('', TeacherListView.as_view(), name='list_teachers'),
    path('create', TeachersCreateView.as_view(), name='create_teachers'),
    path('update/<int:teacher_id>', TeacherUpdateView.as_view(), name='update_teachers'),
    path('delete/<int:teacher_id>', TeacherDeleteView.as_view(), name='delete_teacher'),
]
