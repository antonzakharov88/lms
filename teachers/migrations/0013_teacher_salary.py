# Generated by Django 3.1.4 on 2021-01-19 20:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0012_remove_teacher_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='teacher',
            name='salary',
            field=models.PositiveIntegerField(default=42),
        ),
    ]
