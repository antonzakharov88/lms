# Generated by Django 3.1.4 on 2020-12-19 19:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0002_auto_20201219_1919'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='email',
            field=models.EmailField(default=None, max_length=84),
        ),
    ]
