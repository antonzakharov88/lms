from django.core.management.base import BaseCommand

from teachers.models import Teacher


class Command(BaseCommand):
    help = 'Create fake teachers' # noqa

    def add_arguments(self, parser):
        parser.add_argument('total', type=int, help='Numbers of creations')

    def handle(self, *args, **options):
        total = options['total']
        Teacher.generate_teachers(total)
