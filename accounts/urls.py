from django.contrib import admin # noqa
from django.urls import path

from accounts.views import AccountRegistrationView, AccountLoginView, AccountLogoutView, AccountUpdateView, \
    AccountPasswordChangeView, AccountDeleteView

app_name = 'accounts'
urlpatterns = [

    path('registration', AccountRegistrationView.as_view(), name='registration'),
    path('login', AccountLoginView.as_view(), name='login'),
    path('logout', AccountLogoutView.as_view(), name='logout'),
    path('profile', AccountUpdateView.as_view(), name='profile'),
    path('password', AccountPasswordChangeView.as_view(), name='password'),
    path('delete', AccountDeleteView.as_view(), name='profile_delete')
    # path('update/<int:student_id>', StudentUpdateView.as_view(), name='update'),
    # path('delete/<int:student_id>', StudentDeleteView.as_view(), name='delete'),

]
