from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
# Create your views here.
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView
from django.views.generic.edit import ProcessFormView

from accounts.forms import AccountRegistrationForm, AccountUpdateForm, AccountProfileUpdateForm


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f"User {form.cleaned_data['username']} has succesfully created")
        return result


class AccountLoginView(LoginView):
    success_url = reverse_lazy('index')
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, f"User {self.request.user} has succesfully logged in")
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logged_out.html'

    def dispatch(self, request, *args, **kwargs):
        messages.info(self.request, f"User {self.request.user} has succesfully logged out")
        return super().dispatch(request, *args, **kwargs)


# class AccountUpdateView(UpdateView):
#     model = User
#     template_name = 'profile.html'
#     success_url = reverse_lazy('index')
#     form_class = AccountUpdateForm
#
#     def get_object(self, queryset=None):
#         return self.request.user
#
#     def form_valid(self, form):
#         result = super().form_valid(form)
#         messages.success(self.request, f" Profile: User {self.request.user} has succesfully updated")
#         return result


class AccountPasswordChangeView(PasswordChangeView):
    template_name = 'password_change.html'
    success_url = reverse_lazy('index')


class AccountUpdateView(ProcessFormView):

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(
            data=request.POST,
            instance=user
        )
        profile_form = AccountProfileUpdateForm(
            data=request.POST,
            files=request.FILES,
            instance=profile
        )

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )


class AccountDeleteView(ProcessFormView):
    def get(self, request, *args, **kwargs):
        user = self.request.user

        return render(request,
                      template_name='profile:delete.html',
                      context={'user': user}
                      )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        user.delete()

        return render(request,
                      template_name='index.html',
                      context={'user': user}
                      )
