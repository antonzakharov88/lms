from django.db import models

from groups.models import Group


class Classrooms(models.Model):
    number = models.CharField(max_length=64, null=False)
    groups = models.ManyToManyField(
        to=Group,
        related_name="classrooms"
    )

    def __str__(self):
        return f'{self.number}'
