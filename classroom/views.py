from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import UpdateView, ListView, CreateView, DeleteView

from classroom.forms import ClassroomsFilter, ClassroomsCreateForm, ClassroomsUpdateForm
from classroom.models import Classrooms


def get_classrooms(request):
    classrooms = Classrooms.objects.all().order_by('-id')

    filter = ClassroomsFilter(        # noqa
        data=request.GET,
        queryset=classrooms
    )
    return render(request=request,
                  template_name='classrooms_list.html',
                  context={'filter': filter}
                  )


def create_classroom(request):
    if request.method == 'POST':

        form = ClassroomsCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('classrooms_list'))

    else:

        form = ClassroomsCreateForm()

    return render(request,
                  template_name='create_classroom.html',
                  context={'form': form}
                  )


def update_classrooms(request, classrooms_id):
    classroom = Classrooms.objects.get(id=classrooms_id)

    if request.method == 'POST':

        form = ClassroomsUpdateForm(
            data=request.POST,
            instance=classroom)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('classrooms_list'))

    else:

        form = ClassroomsUpdateForm(instance=classroom)

    return render(request,
                  template_name='update_classroom.html',
                  context={'form': form,
                           'classrooms': classroom
                           }
                  )


def delete_classrooms(request, classrooms_id):
    classroom = get_object_or_404(Classrooms, id=classrooms_id)

    if request.method == 'POST':
        classroom.delete()

        return HttpResponseRedirect(reverse('classrooms_list'))

    return render(request,
                  template_name='delete_classroom.html',
                  context={'classroom': classroom}
                  )


class ClassroomUpdateView(UpdateView):
    model = Classrooms
    form_class = ClassroomsUpdateForm
    success_url = reverse_lazy('classrooms_list')
    template_name = 'classroom:update.html'
    pk_url_kwarg = 'classrooms_id'


class ClassroomsListView(ListView):
    model = Classrooms
    template_name = "classrooms:list.html"

    def get_filter(self):
        return ClassroomsFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
#        context['students'] = self.get_filter().qs.select_related('group').prefetch_related('headed_group')
        return context


class ClassroomCreateView(CreateView):
    model = Classrooms
    form_class = ClassroomsCreateForm
    success_url = reverse_lazy('classrooms_list')
    template_name = 'classroom:create.html'


class ClassroomDeleteView(DeleteView):
    model = Classrooms
    success_url = reverse_lazy('classrooms_list')
    template_name = 'classroom:delete.html'
    pk_url_kwarg = 'classrooms_id'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)
