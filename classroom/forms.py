from django.forms import ModelForm

import django_filters

from classroom.models import Classrooms


class ClassroomsFilter(django_filters.FilterSet):
    class Meta:
        model = Classrooms
        fields = {
            'number': ['exact'],
            'groups': ['exact'],

        }


class ClassroomsBaseForm(ModelForm):
    class Meta:
        model = Classrooms
        fields = "__all__"


class ClassroomsCreateForm(ClassroomsBaseForm):
    pass


class ClassroomsUpdateForm(ClassroomsBaseForm):
    class Meta(ClassroomsBaseForm.Meta):
        pass
