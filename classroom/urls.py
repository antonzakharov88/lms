
from django.urls import path

from classroom.views import ClassroomUpdateView, ClassroomsListView, ClassroomCreateView, ClassroomDeleteView

urlpatterns = [
    path('', ClassroomsListView.as_view(), name='classrooms_list'),
    path('create', ClassroomCreateView.as_view(), name='create_classroom'),
    path('update/<int:classrooms_id>', ClassroomUpdateView.as_view(), name='update_classrooms'),
    path('delete/<int:classrooms_id>', ClassroomDeleteView.as_view(), name='delete_classrooms'),
]
