from django.forms import ModelForm, ChoiceField

import django_filters

from groups.models import Group


class GroupFilter(django_filters.FilterSet):
    class Meta:
        model = Group
        fields = {
            'name': ['exact', 'icontains'],
            'group_teacher': ['exact']

        }


class GroupBaseForm(ModelForm):
    class Meta:
        model = Group
        fields = "__all__"


class GroupCreateForm(GroupBaseForm):
    pass


class GroupUpdateForm(GroupBaseForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        students = [(st.id, st.full_name()) for st in self.instance.students.all()]
        self.headman = ChoiceField(choices=students)
