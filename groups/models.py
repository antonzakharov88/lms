from django.db import models


# Create your models here.


class Group(models.Model):
    name = models.CharField(max_length=64, null=False)
    headman = models.OneToOneField(
        to='students.Student',
        on_delete=models.SET_NULL,
        null=True,
        related_name="headed_group"
    )

    def __str__(self):
        return f'{self.name}'
