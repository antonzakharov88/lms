from copy import copy

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic import UpdateView, ListView, CreateView, DeleteView

from groups.forms import GroupCreateForm, GroupUpdateForm, GroupFilter
from groups.models import Group


def get_groups(request):
    groups = Group.objects.all().select_related('headman', 'course_group').order_by('-id')

    filter = GroupFilter(  # noqa
        data=request.GET,
        queryset=groups
    )
    return render(request=request,
                  template_name='groups_list.html',
                  context={'filter': filter}
                  )


def create_group(request):
    if request.method == 'POST':

        form = GroupCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('groups_list'))

    else:

        form = GroupCreateForm()

    return render(request,
                  template_name='groups_create.html',
                  context={'form': form}
                  )


def update_group(request, group_id):
    group = Group.objects.get(id=group_id)

    if request.method == 'POST':

        form = GroupUpdateForm(
            data=request.POST,
            instance=group)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('groups_list'))

    else:

        form = GroupUpdateForm(instance=group)

    return render(request,
                  template_name='groups_update.html',
                  context={'form': form,
                           'group': group,
                           'students': group.students.select_related('group').all,
                           }
                  )


def delete_group(request, group_id):
    group = get_object_or_404(Group, id=group_id)

    if request.method == 'POST':
        group.delete()

        return HttpResponseRedirect(reverse('groups_list'))

    return render(request,
                  template_name='groups_delete.html',
                  context={'group': group}
                  )


class GroupUpdateView(UpdateView):
    model = Group
    form_class = GroupUpdateForm
    success_url = reverse_lazy('groups_list')
    template_name = 'groups:update.html'
    pk_url_kwarg = 'group_id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.select_related('group', 'headed_group').all()
        return context


class GroupListView(LoginRequiredMixin, ListView):
    model = Group
    template_name = "groups:list.html"
    login_url = reverse_lazy('accounts:login')
    paginate_by = 5
    context_object_name = 'groups'

    def get_filter(self):
        return GroupFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs.select_related('course_group', 'headman')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        context['query_params'] = self.request.GET.urlencode()
        if 'page' in self.request.GET:
            get_params = copy(self.request.GET)
            del get_params['page']
            context['query_params'] = get_params.urlencode()
        return context


class GroupCreateView(CreateView):
    model = Group
    form_class = GroupCreateForm
    success_url = reverse_lazy('groups_list')
    template_name = 'groups:create.html'


class GroupDeleteView(DeleteView):
    model = Group
    success_url = reverse_lazy('groups_list')
    template_name = 'groups:delete.html'
    pk_url_kwarg = 'group_id'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)
