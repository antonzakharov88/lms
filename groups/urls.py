
from django.urls import path

from groups.views import GroupUpdateView, GroupListView, GroupCreateView, GroupDeleteView

urlpatterns = [
    path('', GroupListView.as_view(), name='groups_list'),
    path('create', GroupCreateView.as_view(), name='create_group'),
    path('update/<int:group_id>', GroupUpdateView.as_view(), name='update_group'),
    path('delete/<int:group_id>', GroupDeleteView.as_view(), name='delete_group'),
]
