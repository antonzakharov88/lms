import re

from django.core.exceptions import ValidationError


def validate_prohibited_email(value):
    BLACK_LIST = ['mail.ru', 'inbox.ru', 'index.ua', 'mail.ua', 'yandex.ru', 'yandex.ua', 'list.ru']  # noqa
    _, _, email_domain = value.partition('@')
    if email_domain in BLACK_LIST:
        raise ValidationError(f'Email coudn\'t be at such domain {BLACK_LIST}')
    return value


def validate_phone_number(value):
    SHORT_LENGTH = 13  # noqa

    pattern = '(\(\d\d\d\)|\+\d\d\(\d\d\d\))\d\d\d\-\d\d\d\d'  # noqa

    if not re.match(pattern, value):
        raise ValidationError('Phone number is not correct')

    if len(value) == SHORT_LENGTH:
        value = '+38' + value
    return value
