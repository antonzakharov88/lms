import time

from core.models import Logger


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response


class PerfTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        start_time = time.time()

        response = self.get_response(request)

        execution_time = time.time() - start_time

        if request.user.is_authenticated:
            log = Logger(
                user=request.user,
                path=request.path,
                execution_time=execution_time,
                query_params=request.GET.urlencode(),
                info='Time logged'
            )
            log.save()

        return response
