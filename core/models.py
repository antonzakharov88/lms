import random

from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from faker import Faker

from core.validators import validate_prohibited_email, validate_phone_number


# Create your models here.
class Person(models.Model):
    class Meta:
        abstract = True

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    age = models.IntegerField(null=True, default=42, validators=[
        MinValueValidator(10),
        MaxValueValidator(100),
    ])

    email_field = models.EmailField(max_length=64, default="email.com", validators=[
        validate_prohibited_email
    ])
    phone_number = models.CharField(null=False, max_length=20, default='', validators=[
        validate_phone_number
    ])

    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.age},' \
               f' {self.email_field}, {self.phone_number}'

    @classmethod
    def _generate(cls):
        faker = Faker()
        object = cls( # noqa
            first_name=faker.first_name(),
            last_name=faker.last_name(),
            age=random.randint(15, 105)
        )
        object.save()
        return object

    @classmethod
    def generate(cls, count):
        for _ in range(count):
            cls._generate()

    def full_name(self):
        return f'{self.first_name} {self.last_name}'


class Logger(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    path = models.CharField(max_length=128)
    create_date = models.DateTimeField(auto_now_add=True)
    execution_time = models.FloatField()
    query_params = models.CharField(max_length=64, null=True)
    info = models.CharField(max_length=128, null=True)
